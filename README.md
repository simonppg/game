# game
programación de juegos con SDL2

```plantuml
@startuml
Class Car {
  - make: String
  - model: String
  + drive()
}

Car *-- Engine

Class Engine {
  - cylinders: int
  - horsepower: int
  + start()
  + stop()
}
@enduml

```
